import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpParams , HttpErrorResponse} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  myAPIUrl: string;
  myBearerToken: string;
  myUsername: string;
  myPassword: string;
  mySite: string;
  mySiteId: string;
  mySites: any[] = [];
  
  myOrganizerId: string;
  myRoomsId: string[];
  meetingsIdCreated: string[] = [];
  error: string;
  minDate: Date;

  constructor(private http: HttpClient) {}

  getHeaders () {
    return new HttpHeaders({
      'Authorization': this.myBearerToken
    })
  }

  getMyUserInfos(myUserName: string, myPassword: string) {
    const httpOptions = {
      headers: {
        'Authorization': this.myBearerToken,
      },
      params: {
        userName: myUserName,
        pinCode: myPassword
      }
    };
    return this.http.get<any>(this.myAPIUrl + 'User', httpOptions)
  }

  getAllSitesInfos() {
    const httpOptions = {
      headers: {
        'Authorization': this.myBearerToken,
        //'Content-Type': 'text/json'
      }
    }
    return this.http.get<any>(this.myAPIUrl + 'Site', httpOptions)
  }

  getAllRoomsInfos(mySiteId: string) {
    const httpOptions = {
      headers: {
        'Authorization': this.myBearerToken,
      },
      params: {
        siteId: mySiteId
      }
    };
    return this.http.get<any>(this.myAPIUrl + 'Room', httpOptions)
  }

  formatDate(date: any) {
    var d = date,
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();
    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }
    return [year, month, day].join('-');
  }

  createMeeting(meetingsToCreate: any) {
    return this.http.post<any>(this.myAPIUrl + 'Meeting', meetingsToCreate, {headers: this.getHeaders(), observe: 'response'})
  }

  deleteMeeting(meetingId: string) {
    const httpOptions = {
      headers: {
        'Authorization': this.myBearerToken,
      }
    };
    return this.http.delete<any>(this.myAPIUrl + 'Meeting/' + meetingId, httpOptions)
  }
}