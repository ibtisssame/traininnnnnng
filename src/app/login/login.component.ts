﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '@app/_services';
import { MeetingService } from '@app/_services';


@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private meetingservice: MeetingService,
    ) { 
        // redirect to home if already logged in
        /*if (this.authenticationService.currentUserValue) { 
            console.log('  already auth')
            this.router.navigateByUrl('/home');
        }*/
    }

    defaultLogin = [{
        portail: 'Demo Tech',
        apiurl: 'https://demo.safeware.fr/tech/HamiltonAppsAPI/api/',
        apikey: 'MEI97ZZ8POQFZZ2BIBWJPRNLSLPZ',
        username: 'taffin',
        password: '2206'
       },
       {
        portail: 'Demo UK',
        apiurl: 'https://demo.myhamiltonapps.com/HamiltonAppsAPI/api/',
        apikey: 'MDJSMZZ4CTUPZZ2BIC05S145X6YP',
        username: 'taffin',
        password: '2206'
       },
       {
        portail: 'Trial UK',
        apiurl: 'https://trial.myhamiltonapps.com/HamiltonAppsAPI/api/',
        apikey: 'MDJSMZZ4CTUPZZ2BIC05S145X6YP',
        username: 'taffin',
        password: '2206'
       }]

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            portail: ['', Validators.required],
            apiurl: ['', Validators.required],
            apikey: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.loginForm.get('portail').valueChanges.subscribe((val) => {
            const selectedDefaultLogin = this.defaultLogin.find(defaultLogin => {
                return defaultLogin.portail === val;
            });
            this.loginForm.get('apikey').setValue(selectedDefaultLogin.apikey);
            this.loginForm.get('apiurl').setValue(selectedDefaultLogin.apiurl);
            this.loginForm.get('username').setValue(selectedDefaultLogin.username);
            this.loginForm.get('password').setValue(selectedDefaultLogin.password);
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.apiurl.value, this.f.apikey.value, this.f.username.value, this.f.password.value)
            .subscribe(
                async data => {
                    this.meetingservice.mySites = [];
                    this.meetingservice.myAPIUrl = this.f.apiurl.value;
                    this.meetingservice.myBearerToken = data.token_type + ' ' + data.access_token;
                    this.meetingservice.myUsername = this.f.username.value;
                    this.meetingservice.myPassword = this.f.password.value;
                    //this.meetingservice.mySite = this.f.site.value;
                    console.log('myAPIUrl : ', this.meetingservice.myAPIUrl);
                    console.log('myBearerToken : ', this.meetingservice.myBearerToken);
                    console.log('myUsername : ', this.meetingservice.myUsername);
                    console.log('myPassword : ', this.meetingservice.myPassword);
                    //console.log('mySite : ', this.meetingservice.mySite);

                    interface site {
                        description: string;
                        siteId: string;
                    }

                    let promises = [];
                    let newPromise = this.meetingservice.getAllSitesInfos().toPromise()
                    .then((allSites) => {
                        for (let i = 0; i < allSites.length; i++) {
                            let newSite: site = {
                                description: allSites[i].description,
                                siteId: allSites[i].siteId
                            };
                            this.meetingservice.mySites.push(newSite);
                        }
                    }).catch(err => {
                        this.meetingservice.mySites.push(err.error.message);
                        console.log(err.error.message);
                    })
                    promises.push(newPromise);
                    Promise.all(promises).then(res => {
                       const today = new Date()
                       this.meetingservice.minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
                       
                       this.router.navigateByUrl('/home');
                    })
                },
                error => {
                    console.log('received err ', error.error.error);
                    this.error = error.error.error;
                    if (!this.error) {
                        this.error = 'Unknown error'
                    }
                    this.loading = false;
                });
    }
}