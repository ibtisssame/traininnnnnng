﻿import { Component, Input } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from '@app/_models';
import { UserService } from '@app/_services';
import { MeetingService } from '@app/_services';
import { AuthenticationService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import 'rxjs/add/operator/toPromise';

var j = 1;

//import * as meetingsInfos from  '../../assets/meetingsInfos.json';
declare var require: any
export const meetingsInfos = require('../../assets/meetingsInfos.json')

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})
export class HomeComponent {
    loading = false;
    submitted = false;
    homeForm: FormGroup;
    loadingCreate = false;
    loadingDelete = false;
    users: User[];
    messageRoomsFound = '';
    messageMeetingsCreatedSuccess = '';
    messageMeetingsCreatedFail = '';
    messageMeetingsDeletedSuccess = '';
    messageMeetingsDeletedFail = '';

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
        private meetingservice: MeetingService,
        private authenticationService: AuthenticationService,
    ) { }

    mySites = this.meetingservice.mySites;

    ngOnInit() {
        this.homeForm = this.formBuilder.group({
            site: ['', Validators.required],
            startDate: ['', Validators.required],
        });
    }

    get f() { return this.homeForm.controls; }


    getDates() {
        const dates: Date[] = [];
        dates.push(this.f.startDate.value);
        for (let i = 1; i < 5; i++) {
            let nextDay = new Date(dates[i - 1]);
            nextDay.setDate(nextDay.getDate() + 1)
            dates.push(nextDay);
        }
        for (let i = 0; i < 5; i++) {
            let day = dates[i].getDay();
            if (i === 0 && day === 0) {
                for (let j = i; j < 5; j++) {
                    dates[j].setDate(dates[j].getDate() + 1)
                }
                break;
            }
            if (day === 6) {
                for (let j = i; j < 5; j++) {
                    dates[j].setDate(dates[j].getDate() + 2)
                }
                break;
            }
        }
        return dates
    }

    async createMeetings() {
        this.submitted = true;
        // stop here if form is invalid
        
        if (this.homeForm.invalid) {
            return;
        }
        this.loading = true;
        this.loadingCreate = true;
        
        const dates = this.getDates();
        var datesString: string[] = [];
        for (let i = 0; i < dates.length; i++) {
            datesString.push(this.meetingservice.formatDate(dates[i]));
        }
        
        console.log('this.f.site.value : ', this.f.site.value);

        //myOrganizerId
        const allUserInfos = await this.meetingservice.getMyUserInfos(this.meetingservice.myUsername, this.meetingservice.myPassword).toPromise();
        this.meetingservice.myOrganizerId = allUserInfos.organizerId;
        console.log('myOrganizerId : ', this.meetingservice.myOrganizerId);

        //myRoomsId
        const allRoomsInfos = await this.meetingservice.getAllRoomsInfos(this.f.site.value).toPromise()
        this.meetingservice.myRoomsId = allRoomsInfos.map(element => element.roomId)
        this.messageRoomsFound = this.meetingservice.myRoomsId.length + ' rooms found.';
        console.log(this.meetingservice.myRoomsId.length + ' rooms found.');

        //if (this.meetingservice.myRoomsId.length == 0) {
        //    'please create at least one room.'
        //    return;
        //}

        //meetingsToCreate
        interface meeting {
            description: string;
            organizerId: string;
            roomId: string;
            meetingEndDate: string;
            meetingStartDate: string;
        }
        var meetingsToCreate: meeting[] = [];
        for (var i = 0; i < meetingsInfos.length; i++) {
            let index = meetingsInfos[i].Numero_salle;
            if (index <= this.meetingservice.myRoomsId.length) {
                let myMeetingStartDate = datesString[meetingsInfos[i].Jour_de_la_semaine - 1].concat('T', meetingsInfos[i].Heure_de_debut);
                let myMeetingEndDate = datesString[meetingsInfos[i].Jour_de_la_semaine - 1].concat('T', meetingsInfos[i].Heure_de_fin);
                let newMeeting: meeting = {
                    description: meetingsInfos[i].Titre,
                    organizerId: this.meetingservice.myOrganizerId,
                    roomId: this.meetingservice.myRoomsId[index - 1],
                    meetingEndDate: myMeetingEndDate,
                    meetingStartDate: myMeetingStartDate
                };
                meetingsToCreate.push(newMeeting);
            }
        }
        console.log(meetingsToCreate.length + '/' + meetingsInfos.length + ' meetings can be created.');

        //createMeetings
        let promises = [];
        let meetingsFailedToCreate: number = 0;
        for (var i = 0; i < meetingsToCreate.length; i++) {
            let promise = this.meetingservice.createMeeting(meetingsToCreate[i]).toPromise()
            .then((meetingCreated) => {
                this.messageMeetingsCreatedSuccess = j + '/' + meetingsToCreate.length + ' meetings created.';
                console.log(this.messageMeetingsCreatedSuccess)
                j++;
                this.meetingservice.meetingsIdCreated.push(meetingCreated.body.meetingId);
            }).catch(err => {
                meetingsFailedToCreate++;
                this.messageMeetingsCreatedFail = meetingsFailedToCreate + '/' + meetingsToCreate.length + ' meetings failed to create : ' + err.error.message + '.';
                console.log(err.error.message);
            })
            promises.push(promise);
        }
        Promise.all(promises).then(res => {
            setTimeout(() => {
                this.messageMeetingsCreatedSuccess = '';
                this.messageMeetingsCreatedFail = '';
                this.messageRoomsFound = '';
            }, 3000);
            if (this.meetingservice.meetingsIdCreated.length != 0) {
                console.log('meetingsIdCreated : ', this.meetingservice.meetingsIdCreated);
            } else {
                console.log('0/' + meetingsToCreate.length + ' meetings created.');
            }
            j = 1;
            this.loadingCreate = false;
        })
    }

    async deleteMeetings() {
        let meetingsFailedToDelete: number = 0;
        this.loadingDelete = true;
        let promises = [];
        for (var i = 0; i < this.meetingservice.meetingsIdCreated.length; i++) {
            let promise = this.meetingservice.deleteMeeting(this.meetingservice.meetingsIdCreated[i]).toPromise()
                .then(() => {
                    this.messageMeetingsDeletedSuccess = j + '/' + this.meetingservice.meetingsIdCreated.length + ' meetings deleted.';
                    console.log(this.messageMeetingsDeletedSuccess);
                    j++;
                }).catch(err => {
                    meetingsFailedToDelete++;
                    this.messageMeetingsDeletedFail = meetingsFailedToDelete + '/' + this.meetingservice.meetingsIdCreated.length + ' meetings failed to create : ' + err.error.message + '.';
                    console.log(err.error.message);
                })
            promises.push(promise);
        }
        Promise.all(promises).then(res => {
            setTimeout(() => {
                this.messageMeetingsDeletedSuccess = '';
                this.messageMeetingsDeletedFail = '';
            }, 3000);
            console.log(this.meetingservice.meetingsIdCreated.length + ' meetings deleted.');
            if (meetingsFailedToDelete != 0) {
                console.log(meetingsFailedToDelete + ' meetings failed to delete.');
            }
            this.meetingservice.meetingsIdCreated = [];
            j = 1;
            this.loadingDelete = false;
        })
    }
}