﻿export class User {
    id: number;
    apiurl: string;
    apikey: string;
    username: string;
    password: string;
    authdata?: string;
}